//
//  TableViewController.swift
//  TableViewEmbedded
//
//  Created by ivk on 20/10/2020.
//  Copyright © 2020 irr. All rights reserved.



import UIKit

class TableViewController: UITableViewController {
    
    var kindModel : [ (String, String, String)] = []
    
    override func viewDidLoad() {
        
        kindModel = [
                    ("Radical Kindness!", "horizontalDesk", ""),
                    ("Releases Feel-Good Hormones", "pexels-roman-carey-734658", "Photo by Roman Carey from Pexels"),
                    ("Good For Your\nHeart & Mind", "pexels-streetwindy-3101214", "Photo by Streetwindy from Pexels"),
                    ("Reverses Stress & Depression", "pexels-cottonbro-4888440", "Photo by cottonbro from Pexels"),
                    ("Improves Relationships", "pexels-suraphat-nueaon-933605", "Photo by Suraphat Nuea-on from Pexels"),
                    ("Kindness Is Contagious", "pexels-agostino-toselli-4060475", "Photo by Agostino Toselli from Pexels"),
                    ("Be Kind, No Matter What!", "pexels-pixabay-36785", ""),
                    ]
        
super.viewDidLoad()
var nib = UINib(nibName: "MyTableViewCell", bundle: nil)
self.tableView.register(nib, forCellReuseIdentifier: "MyTableViewCell")
        
        nib = UINib(nibName: "TopTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "TopTableViewCell")
    }
    
    // Sets the number of rows!!
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return kindModel.count
    }
    
    // Height for eahc row!
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        // First row, the header of the table, is higher than the others!
        if (indexPath.row == 0){
            return 227
        }else{
            return 120
        }
        
    }
    
    func tableViewBodyKind (indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell", for: indexPath) as! MyTableViewCell
        cell.labelName.text = kindModel[indexPath.row].0
        
        let colorStatus: UIColor?
        if (indexPath.row  % 3 == 0){
            colorStatus = .red
        }else{
            colorStatus = .systemBlue
        }
        cell.imageStatus.backgroundColor = colorStatus
        cell.imagePokerCard.image = UIImage(named: kindModel[indexPath.row].1)
        
        return cell
        
    }
    func tableTop(indexPath: IndexPath) -> UITableViewCell {
                    
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopTableViewCell", for: indexPath) as! TopTableViewCell
        cell.imageTitle.image = UIImage(named: kindModel[indexPath.row].1)
        cell.labelTitle.text = kindModel[indexPath.row].0
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell: UITableViewCell
        if (indexPath.row == 0){
            cell = tableTop(indexPath: indexPath)
        }else{
            cell = tableViewBodyKind(indexPath: indexPath)
        }
        return cell
    }
    
    
    // selected row!
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        let vc = ViewController()
//        vc.view.backgroundColor = .red
//        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    // Deselected Row!
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
}
