//
//  MyTableViewCell.swift
//  TableViewEmbedded
//
//  Created by ivk on 20/10/2020.
//  Copyright © 2020 irr. All rights reserved.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet var labelName: UILabel!
    @IBOutlet var imageStatus: UIImageView!
    @IBOutlet var btDetails: UIButton!
    @IBOutlet var imagePokerCard: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print ("\(#function), (self.accessoryType) = \(self.accessoryType)")
        // All cells with .none at the beginning
        self.accessoryType = .none

        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
        print ("\(#function) \(selected)")
        // Adding a checkmark accesory for the selected cell
        if (selected == true){
            self.accessoryType = .checkmark
        }else{

            self.accessoryType = .detailDisclosureButton
        }
    }
}
