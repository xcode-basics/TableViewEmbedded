//
//  TopTableViewCell.swift
//  TableViewEmbedded
//
//  Created by ivk on 22/10/2020.
//  Copyright © 2020 irr. All rights reserved.
//

import UIKit

class TopTableViewCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var imageTitle: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
